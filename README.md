Tanuki
======
[![build status](https://gitlab.com/zj/Tanuki/badges/master/build.svg)](https://gitlab.com/zj/Tanuki/commits/master)
[![Coverage Status](https://coveralls.io/repos/github/ZJvandeWeg/Tanuki/badge.svg?branch=coveralls)](https://coveralls.io/github/ZJvandeWeg/Tanuki?branch=master)
[![Inline docs](http://inch-ci.org/github/ZJvandeWeg/tanuki.svg)](http://inch-ci.org/github/ZJvandeWeg/tanuki)

Elixir wrapper for GitLab. You'll need your private token found under <Gitlab Instance URL>profile/account or an OAuth token (requesting this token is not supported through this library).

## How to use
This library depends on `HTTPoison`, thus at some point during previous to make some calls the your choosen GitLab endpoint, you should have started `HTTPoison`.

Then:
```elixir
# first you'll need your private token and create a %Tanuki.Client
iex> client = Tanuki.Client.new("PrivateToken", "https://customendpoint.tld/api/v3/")
%Tanuki.Client%{private_token: "PrivateToken", endpoint: "https://customendpoint.tld/api/v3/"}

iex> Tanuki.Users.Emails.create(client, %{email: "your@email.tld"})
# Response struct
```

Please browse through the test folder to see more examples.

## Docs
All the API's have extensive documentation found on [HexDoc]([http://hexdocs.pm/tanuki/api-reference.html) and offcourse; [GitLab API docs](http://doc.gitlab.com/ce/api/).

## Contributions
If there is any issue, please report it. Mention what version your GitLab endpoint is running.

Some endpoints are still missing, if you really mis those, please contribute!
