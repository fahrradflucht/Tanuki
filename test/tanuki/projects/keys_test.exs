defmodule Tanuki.Project.KeysTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @client Tanuki.Client.new(%{private_token: "g8zWsDJqs5LCn9-JRmJh"}, "http://localhost:8080/api/v3/")
  alias Tanuki.Projects.Keys

  setup_all do
    ExVCR.Config.cassette_library_dir("test/fixtures/vcr_cassettes")
    HTTPoison.start
  end

  test "Keys.list/1" do
    use_cassette "project_keys_list" do
      assert Keys.list(1, @client) |> Enum.empty?
    end
  end
end
