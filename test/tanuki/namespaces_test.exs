defmodule Tanuki.NameSpacesTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @client Tanuki.Client.new(%{private_token: "A28e6ZeJHNJQLN6r6yTv"}, "http://localhost:3000/api/v3/")
  alias Tanuki.NameSpaces

  setup_all do
    ExVCR.Config.cassette_library_dir("test/fixtures/vcr_cassettes")
    HTTPoison.start
  end

  test "NameSpaces.list/1" do
    use_cassette "namespaces_list" do
      assert NameSpaces.list(@client) |> Enum.count > 5
    end
  end
end
