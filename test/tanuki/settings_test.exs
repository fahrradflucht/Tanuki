defmodule Tanuki.SettingsTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @client Tanuki.Client.new(%{private_token: "g8zWsDJqs5LCn9-JRmJh"}, "http://localhost:3000/api/v3/")
  alias Tanuki.Settings

  setup_all do
    ExVCR.Config.cassette_library_dir("test/fixtures/vcr_cassettes")
    HTTPoison.start
  end

  test "Settings.list/1" do
    use_cassette "settings_list" do
      assert Settings.list(@client).default_projects_limit == 10
    end
  end

  test "Settings.create/3" do
    settings = %{default_projects_limit: 15}
    use_cassette "settings_modify" do
      assert Settings.modify(@client, settings)
    end
  end
end
