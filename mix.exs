defmodule Tanuki.Mixfile do
  use Mix.Project

  def project do
    [app: :tanuki,
     version: "0.2.0",
     elixir: "~> 1.0",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     description: description,
     package: package,
     deps: deps,
     test_coverage: [tool: ExCoveralls],
     preferred_cli_env: [
        vcr: :test, "vcr.delete": :test, "vcr.check": :test, "vcr.show": :test,
        "coveralls": :test, "coveralls.detail": :test, "coveralls.post": :test
      ]
   ]
  end

  # Configuration for the OTP application
  #
  # Type `mix help compile.app` for more information
  def application do
    [applications: [:logger, :httpoison]]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type `mix help deps` for more examples and options
  defp deps do
    [ {:httpoison,  "~> 0.8"},               # HTTP requests
      {:poison,     "~> 2.1"},               # JSON encoding/
      {:earmark, ">= 0.0.0", only: :dev},    # Docs
      {:ex_doc, "~> 0.10", only: :dev},      # Docs
      {:exvcr, "~> 0.7", only: :test},
      {:inch_ex, "~> 0.5", only: :docs},     # Doc coverage
      {:excoveralls, "~> 0.4", only: :test}
    ]
  end

  defp package do
   [ maintainers: ["Zeger-Jan van de Weg"],
     licenses: ["MIT"],
     links: %{ "Github" => "https://github.com/ZJvandeWeg/Tanuki",
               "GitLab (Mirror)" => "https://gitlab.com/zj/Tanuki"} ]
 end

 defp description do
   """
   GitLab API wrapper in Elixir, named after GitLabs mascot
   """
 end
end
