defmodule Tanuki.Projects do
  @doc """
  GET /projects

  Get a list of projects accessible by the authenticated user.

  Parameters:
    - archived (optional) - if passed, limit by archived status
    - order_by (optional) - Return requests ordered by id, name, path, created_at, updated_at or last_activity_at fields. Default is created_at
    - sort (optional) - Return requests sorted in asc or desc order. Default is desc
    - search (optional) - Return list of authorized projects according to a search criteria
  """
  def list(client, params \\ []), do: Tanuki.get("projects", client, params)

  @doc """
  GET /projects/search/:query

  Search for projects by name which are accessible to the authenticated user.

  Parameters:
  - per_page (optional) - number of projects to return per page
  - page (optional) - the page to retrieve
  - order_by (optional) - Return requests ordered by id, name, created_at or last_activity_at fields
  - sort (optional) - Return requests sorted in asc or desc order

  """
  def search(query, client, params \\ []), do: Tanuki.get("projects/search/#{query}", client, params)

  @doc """
  GET /projects/owned

  Get a list of projects which are owned by the authenticated user.

  Parameters:
    - archived (optional) - if passed, limit by archived status
    - order_by (optional) - Return requests ordered by id, name, path, created_at, updated_at or last_activity_at fields. Default is created_at
    - sort (optional) - Return requests sorted in asc or desc order. Default is desc
    - search (optional) - Return list of authorized projects according to a search criteria
  """
  def owned(client, params \\ []), do: Tanuki.get("projects/owned", client, params)

  @doc """
  GET /projects/starred

  Get a list of projects which are starred by the authenticated user.

  Parameters:
    - archived (optional) - if passed, limit by archived status
    - order_by (optional) - Return requests ordered by id, name, path, created_at, updated_at or last_activity_at fields. Default is created_at
    - sort (optional) - Return requests sorted in asc or desc order. Default is desc
    - search (optional) - Return list of authorized projects according to a search criteria
  """
  def starred(client, params \\ []), do: Tanuki.get("projects/starred", client, params)

  @doc """
  GET /projects/all

  Get a list of all GitLab projects (admin only).

  Parameters:
    - archived (optional) - if passed, limit by archived status
    - order_by (optional) - Return requests ordered by id, name, path, created_at, updated_at or last_activity_at fields. Default is created_at
    - sort (optional) - Return requests sorted in asc or desc order. Default is desc
    - search (optional) - Return list of authorized projects according to a search criteria
  """
  def list_all(client, params \\ []), do: Tanuki.get("projects/all", client, params)

  @doc """
  GET /projects/:id

  Get a specific project, identified by project ID or NAMESPACE/PROJECT_NAME, which is owned by the authenticated user. If using namespaced projects call make sure that the NAMESPACE/PROJECT_NAME is URL-encoded, eg. /api/v3/projects/diaspora%2Fdiaspora (where / is represented by %2F).
  """
  def find(id, client), do: Tanuki.get("projects/#{id}", client)

  @doc """
  GET /projects/:id/events

  Get the events for the specified project. Sorted from newest to latest

  id - can be either the integer id or NAMESPACE/PROJECT_NAME
  """
  def events(id, client), do: Tanuki.get("projects/#{id}/events", client)

  @doc """
  POST /projects

  Creates a new project owned by the authenticated user.

  Parameters:
  - name (required) - new project name
  - path (optional) - custom repository name for new project. By default generated based on name
  - namespace_id (optional) - namespace for the new project (defaults to user)
  -  description (optional) - short project description
  -  issues_enabled (optional)
  -  merge_requests_enabled (optional)
  -  builds_enabled (optional)
  -  wiki_enabled (optional)
  -  snippets_enabled (optional)
  -  public (optional) - if true same as setting visibility_level = 20
  -  visibility_level (optional)
  -  import_url (optional)
  """
  def create(client, params), do: Tanuki.post("projects", client, params)

  @doc """
  POST /projects/user/:uid

  Creates a new project owned by the specified user. Available only for admins.

  Parameters:
  - name (required) - new project name
  - path (optional) - custom repository name for new project. By default generated based on name
  - namespace_id (optional) - namespace for the new project (defaults to user)
  - description (optional) - short project description
  - issues_enabled (optional)
  - merge_requests_enabled (optional)
  - builds_enabled (optional)
  - wiki_enabled (optional)
  - snippets_enabled (optional)
  - public (optional) - if true same as setting visibility_level = 20
  - visibility_level (optional)
  - import_url (optional)
  """
  def create_for_user(user_id, client, params), do: Tanuki.post("projects/user/#{user_id}", client, params)

  @doc """
  PUT /projects/:id

  Updates an existing project

  Parameters:
  - name (required) - new project name
  - path (optional) - custom repository name for new project. By default generated based on name
  - namespace_id (optional) - namespace for the new project (defaults to user)
  - description (optional) - short project description
  - issues_enabled (optional)
  - merge_requests_enabled (optional)
  - builds_enabled (optional)
  - wiki_enabled (optional)
  - snippets_enabled (optional)
  - public (optional) - if true same as setting visibility_level = 20
  - visibility_level (optional)
  """
  def modify(id, client, params), do: Tanuki.put("projects/#{id}", client, params)

  @doc """
  DELETE /projects/:id

  Removes a project including all associated resources (issues, merge requests etc.)
  """
  def delete(id, client), do: Tanuki.delete("projects/#{id}", client)

  @doc """
  POST /projects/fork/:id

  Forks a project into the user namespace of the authenticated user.
  """
  def fork(id, client), do: Tanuki.post("projects/fork/#{id}", client)

  @doc """
  POST /projects/:id/uploads

  Uploads a file to the specified project to be used in an issue or merge request description, or a comment.
   Parameters:
   - file (required) - the file to be uploaded
  """
  def upload(id, client, params), do: Tanuki.post("projects/#{id}/uploads", client, params)

  @doc """
  POST /projects/:id/fork/:forked_from_id

  Create a forked from/to relation between existing projects.
  """
  def create_fork_relation(id, fork_from, client), do: Tanuki.post("projects/#{id}/fork/#{fork_from}")

  @doc """
  POST /projects/:id/fork/:forked_from_id

  Delete an existing forked from relationship
  """
  def create_fork_relation(id, client), do: Tanuki.delete("projects/#{id}/fork")

  @doc """
  POST /projects/:id/share

  Allow to share project with group.

  Parameters:
  - group_id (required) - The ID of a group
  - group_access (required) - Level of permissions for sharing
  """
  def share(id, client, params), do: Tanuki.post("projects/#{id}/share")
end
