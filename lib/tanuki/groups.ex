defmodule Tanuki.Groups do
  @doc """
  GET /groups

  Get a list of groups. (As user: my groups, as admin: all groups)
  """
  def list(client), do: Tanuki.get("groups", client)

  @doc """
  GET /groups/:id/projects

  Get a list of projects in this group.

  Parameters:
  - archived (optional) - if passed, limit by archived status
  - order_by (optional) - Return requests ordered by id, name, path, created_at, updated_at or last_activity_at fields. Default is created_at
  - sort (optional) - Return requests sorted in asc or desc order. Default is desc
  - search (optional) - Return list of authorized projects according to a search criteria
  - ci_enabled_first - Return projects ordered by ci_enabled flag. Projects with enabled GitLab CI go first
  """
  def projects(id, client, params \\ []), do: Tanuki.get("groups/#{id}/projects", client, params)

  @doc """
  GET /groups/:id

  Get all details of a group.
  """
  def find(id, client), do: Tanuki.get("groups/#{id}", client)

  @doc """
  PUT /groups/:id

  Updates a project group. Available only for users who can manage this group.

  Parameters:
  - name (required) - The name of the group
  - path (required) - The path of the group
  - description (optional) - The group's description
  - membership_lock (optional, boolean) - Prevent adding new members to project membership within this group
  - share_with_group_lock (optional, boolean) - Prevent sharing a project with another group within this group
  """
  def modify(id, client, params), do: Tanuki.put("groups/#{id}", client, params)

  @doc """
  POST /groups

  Creates a new project group. Available only for users who can create groups.

  Parameters:
  - name (required) - The name of the group
  - path (required) - The path of the group
  - description (optional) - The group's description
  """
  def create(client, params), do: Tanuki.post("groups", client, params)

  @doc """
  POST  /groups/:id/projects/:project_id

  Transfer a project to the Group namespace. Available only for admin
  """
  def transfer(id, project_id, client), do: Tanuki.post("groups/#{id}/projects/#{project_id}", client)

  @doc """
  DELETE /groups/:id

  Removes group with all projects inside.
  """
  def delete(id, client), do: Tanuki.delete("users/#{id}", client)
end
