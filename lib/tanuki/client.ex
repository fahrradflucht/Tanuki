defmodule Tanuki.Client do
  defstruct private_token: nil, oauth_token: nil, endpoint: "https://gitlab.com/api/v3/"
  @type client_struct :: %{private_token: binary, endpoint: binary }
                      | %{oauth_token: binary, endpoint: binary }

  @doc """
  Returns a Tanuki struct which is needed for each request.

  iex> Tanuki.Client.new(%{private_token: "private_token"})
  %Tanuki.Client{endpoint: "https://gitlab.com/api/v3/", private_token: "private_token", oauth_token: nil}

  iex> Tanuki.Client.new(%{private_token: "private_token"}, "https://mydomain.tld")
  %Tanuki.Client{endpoint: "https://mydomain.tld/", private_token: "private_token", oauth_token: nil}

  Or when using an OAuth token:

  iex> Tanuki.Client.new(%{oauth_token: "token"})
  %Tanuki.Client{endpoint: "https://gitlab.com/api/v3/", private_token: nil, oauth_token: "token"}
  """
  @spec new(binary)         :: client_struct
  @spec new(binary, binary) :: client_struct
  def new(%{private_token: token}), do: %__MODULE__{ private_token: token }
  def new(%{oauth_token: token}),   do: %__MODULE__{ oauth_token: token }
  def new(%{private_token: token}, endpoint), do: %__MODULE__{ private_token: token, endpoint: normalize(endpoint) }
  def new(%{oauth_token: token}, endpoint), do: %__MODULE__{ oauth_token: token, endpoint: normalize(endpoint) }

  defp normalize(endpoint) do
    if(String.ends_with?(endpoint, "/")) do
      endpoint
    else
      endpoint <> "/"
    end
  end
end
