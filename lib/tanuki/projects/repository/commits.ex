defmodule Tanuki.Projects.Repository.Commits do
  @moduledoc """
  Interact with the commits on a project
  """

  @doc """
  GET /projects/:id/repository/commits

  Get a list of repository commits in a project.

  Parameters:
  - ref_name (optional) - The name of a repository branch or tag or if not given the default branch
  """
  def list(id, client, params \\ []), do: Tanuki.get("projects/#{id}/repository/commits", client, params)

  @doc """
  GET /projects/:id/repository/commits/:sha

  Get a specific commit identified by the commit hash or name of a branch or tag.
  """
  def find(id, sha, client), do: Tanuki.get("projects/#{id}/repository/commits/#{sha}", client)

  @doc """
  GET /projects/:id/repository/commits/:sha/diff

  Get the diff of a commit in a project.
  """
  def diff(id, sha, client), do: Tanuki.get("projects/#{id}/repository/commits/#{sha}/diff", client)

  @doc """
  GET /projects/:id/repository/commits/:sha/comments

  Get the comments of a commit in a project.
  """
  def comments(id, sha, client), do: Tanuki.get("projects/#{id}/repository/commits/#{sha}/comments", client)

  @doc """
  POST /projects/:id/repository/commits/:sha/comments

  Adds a comment to a commit. Optionally you can post comments on a specific line of a commit. Therefor both path, line_new and line_old are required.

  Parameters:
  - note (required) - Text of comment
  - path (optional) - The file path
  - line (optional) - The line number
  - line_type (optional) - The line type (new or old)
  """
  def create_comment(id, sha, client), do: Tanuki.post("projects/#{id}/repository/commits/#{sha}/comments", client)

  @doc """
  GET /projects/:id/repository/commits/:sha/statuses

  Get the statuses of a commit in a project.

  Parameters:
  - ref (optional) - Filter by ref name, it can be branch or tag
  - stage (optional) - Filter by stage
  - name (optional) - Filer by status name, eg. jenkins
  - all (optional) - The flag to return all statuses, not only latest ones
  """
  def status(id, sha, client, params \\ []), do: Tanuki.get("projects/#{id}/repository/commits/#{sha}/statuses", client, params)

  @doc """
  POST /projects/:id/statuses/:sha

  Adds or updates a status of a commit.

  Parameters:
  - state (required) - The state of the status. Can be: pending, running, success, failed, canceled
  - ref (optional) - The ref (branch or tag) to which the status refers
  - name or context (optional) - The label to differentiate this status from the status of other systems. Default: "default"
  - target_url (optional) - The target URL to associate with this status
  - description (optional) - The short description of the status
  """
  def create_status(id, sha, client, params), do: Tanuki.post("projects/#{id}/statuses/#{sha}", client, params)
end
