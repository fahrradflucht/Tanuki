defmodule Tanuki.Projects.Repository.Branches do
  @doc """
  GET /projects/:id/repository/branches

  Lists all branches of a project.
  """
  def list(id, client), do: Tanuki.get("projects/#{id}/repository/branches", client)

  @doc """
  GET /projects/:id/repository/branches/:branch

  Lists a specific branch of a project.
  """
  def find(id, branch_name, client), do: Tanuki.get("projects/#{id}/repository/branches/#{branch_name}", client)

  @doc """
  POST /projects/:id/repository/branches

  Parameters:
  - branch_name (required) - The name of the branch
  - ref (required) - The branch name or commit SHA to create branch from
  """
  def create(id, client, params), do: Tanuki.post("projects/#{id}/repository/branches", client, params)

  @doc """
  DELETE /projects/:id/repository/branches/:branch

  It returns 200 if it succeeds, 404 if the branch to be deleted does not exist or 400 for other reasons. In case of an error, an explaining message is provided.
  """
  def delete(id, branch, client), do: Tanuki.delete("projects/#{id}/repository/branches/#{branch}")

  @doc """
  PUT /projects/:id/repository/branches/:branch/protect

  Protects a single branch of a project.
  """
  def protect(id, branch_name, client), do: Tanuki.put("projects/#{id}/repository/branches/#{branch_name}/protect", client)

  @doc """
  PUT /projects/:id/repository/branches/:branch/unprotect

  Unprotects a single branch of a project.
  """
  def unprotect(id, branch_name, client), do: Tanuki.put("projects/#{id}/repository/branches/#{branch_name}/unprotect", client)
end
