defmodule Tanuki.Projects.Repository.Files do
  @moduledoc """
  Create, read, update and delete repository files using this API
  """

  @doc """
  GET /projects/:id/repository/files

  Allows you to receive information about file in repository like name, size, content. Note that file content is Base64 encoded.

  Parameters:
  -  file_path (required) - Full path to new file. Ex. lib/class.rb
  -  ref (required) - The name of branch, tag or commit
  """
  def find(id, client, params), do: Tanuki.get("projects/#{id}/repository/files", client, params)

  @doc """
  POST /projects/:id/repository/files

  Create new file in repository

  Parameters:
  - file_path (required) - Full path to new file. Ex. lib/class.rb
  - branch_name (required) - The name of branch
  - encoding (optional) - 'text' or 'base64'. Text is default.
  - content (required) - File content
  - commit_message (required) - Commit message
  """
  def create(id, client, params), do: Tanuki.post("projects/#{id}/repository/files", client, params)

  @doc """
  PUT /projects/:id/repository/files

  Update existing file in repository

  If the commit fails for any reason we return a 400 error with a non-specific error message. Possible causes for a failed commit include:

    the file_path contained /../ (attempted directory traversal);
    the new file contents were identical to the current file contents, i.e. the user tried to make an empty commit;
    the branch was updated by a Git push while the file edit was in progress.

  Currently gitlab-shell has a boolean return code, preventing GitLab from specifying the error.

  Parameters:
  - file_path (required) - Full path to file. Ex. lib/class.rb
  - branch_name (required) - The name of branch
  - encoding (optional) - 'text' or 'base64'. Text is default.
  - content (required) - New file content
  - commit_message (required) - Commit message
  """
  def modify(id, client, params), do: Tanuki.put("projects/#{id}/repository/files", client, params)

  @doc """
  DELETE /projects/:id/repository/files

  Delete existing file in repository

  Parameters:
  - file_path (required) - Full path to file. Ex. lib/class.rb
  - branch_name (required) - The name of branch
  - commit_message (required) - Commit message
  """
  def delete(id, client, params), do: Tanuki.delete("projects/#{id}/repository/files", client, params)
end
