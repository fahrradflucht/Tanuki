defmodule Tanuki.Projects.Labels do
  @doc """
  GET /projects/:id/labels

  Get all labels for given project.
  """
  def list(id, client), do: Tanuki.get("projects/#{id}/labels", client)

  @doc """
  POST /projects/:id/labels

  Creates a new label for given repository with given name and color.

  Parameters:
  - name (required) - The name of the label
  - color (required) - Color of the label given in 6-digit hex notation with leading '#' sign (e.g. #FFAABB)
  """
  def create(id, client, params), do: Tanuki.post("projects/#{id}/labels", client, params)

  @doc """
  PUT /projects/:id/labels

  Updates an existing label with new name or now color. At least one parameter is required, to update the label.

  Parameters:
  - name (required) - The name of the existing label
  - new_name (optional) - The new name of the label
  - color (optional) - New color of the label given in 6-digit hex notation with leading '#' sign (e.g. #FFAABB)
  """
  def modify(id, client, params), do: Tanuki.put("projects/#{id}/labels", client, params)

  @doc """
  DELETE /projects/:id/labels

  Deletes a label given by its name.

  Parameters:
  - name (required) - The name of the label to be deleted
  """
  def delete(id, hook_id, client), do: Tanuki.delete("projects/#{id}/labels", client)
end
