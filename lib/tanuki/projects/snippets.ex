defmodule Tanuki.Projects.Snippets do
  @moduledoc """
  Snippets in GitLab can be either private, internal or public. You can set it with the visibility_level field in the snippet.

  Constants for snippet visibility levels are:
  Visibility 	visibility_level 	Description
  Private 	  0 	              The snippet is visible only the snippet creator
  Internal 	  10 	              The snippet is visible for any logged in user
  Public 	    20 	              The snippet can be accessed without any authentication
  """

  @doc """
  GET /projects/:id/snippets

  Get a list of project snippets.
  """
  def list(id, client), do: Tanuki.get("projects/#{id}/snippets", client)

  @doc """
  GET /projects/:id/repository/branches/:branch

  Lists a specific branch of a project.
  """
  def find(id, snippet_id, client), do: Tanuki.get("projects/#{id}/snippets/#{snippet_id}", client)

  @doc """
  POST /projects/:id/snippets

  Creates a new project snippet. The user must have permission to create new snippets.

  Parameters:
  - title (required) - The title of a snippet
  - file_name (required) - The name of a snippet file
  - code (required) - The content of a snippet
  - visibility_level (required) - The snippet's visibility
  """
  def create(id, client, params), do: Tanuki.post("projects/#{id}/snippets", client, params)

  @doc """
  PUT /projects/:id/snippets/:snippet_id

  Updates an existing project snippet. The user must have permission to change an existing snippet.

  Parameters:
  - title (optional) - The title of a snippet
  - file_name (optional) - The name of a snippet file
  - code (optional) - The content of a snippet
  - visibility_level (optional) - The snippet's visibility
  """
  def modify(id, snippet_id, client, params \\ []), do: Tanuki.put("projects/#{id}/snippets/#{snippet_id}", client, params)

  @doc """
  DELETE /projects/:id/snippets/:snippet_id

  Deletes an existing project snippet. This is an idempotent function and deleting a non-existent snippet still returns a 200 OK status code.
  """
  def delete(id, snippet_id, client), do: Tanuki.delete("projects/#{id}/snippets/#{snippet_id}", client)

  @doc """
  GET /projects/:id/snippets/:snippet_id/raw

  Deletes an existing project snippet. This is an idempotent function and deleting a non-existent snippet still returns a 200 OK status code.
  """
  def content(id, snippet_id, client), do: Tanuki.get("projects/#{id}/snippets/#{snippet_id}/raw", client)
end
