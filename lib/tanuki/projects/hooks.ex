defmodule Tanuki.Projects.Hooks do
  @doc """
  GET /projects/:id/hooks

  Get a list of project hooks.
  """
  def list(id, client), do: Tanuki.get("projects/#{id}/hooks", client)

  @doc """
  GET /projects/:id/hooks/:hook_id

  Get a specific hook for a project.
  """
  def find(id, hook_id, client), do: Tanuki.get("projects/#{id}/hooks/#{hook_id}", client)

  @doc """
  POST /projects/:id/hooks

  Adds a hook to a specified project.

  Parameters:
  - url (required) - The hook URL
  - push_events - Trigger hook on push events
  - issues_events - Trigger hook on issues events
  - merge_requests_events - Trigger hook on merge_requests events
  - tag_push_events - Trigger hook on push_tag events
  - note_events - Trigger hook on note events
  - enable_ssl_verification - Do SSL verification when triggering the hook
  """
  def create(id, client, params), do: Tanuki.post("projects/#{id}/hooks", client, params)

  @doc """
  PUT /projects/:id/hooks/:hook_id

  Edits a hook for a specified project.

  Parameters:
  - url (required) - The hook URL
  - push_events - Trigger hook on push events
  - issues_events - Trigger hook on issues events
  - merge_requests_events - Trigger hook on merge_requests events
  - tag_push_events - Trigger hook on push_tag events
  - note_events - Trigger hook on note events
  - enable_ssl_verification - Do SSL verification when triggering the hook
  """
  def modify(id, hook_id, client, params), do: Tanuki.put("projects/#{id}/hooks/#{hook_id}", client, params)

  @doc """
  DELETE /projects/:id/hooks/:hook_id

  Removes a hook from a project. This is an idempotent method and can be called multiple times. Either the hook is available or not.
  """
  def delete(id, hook_id, client), do: Tanuki.delete("projects/#{id}/hooks/#{hook_id}", client)
end
