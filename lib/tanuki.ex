defmodule Tanuki do
  use HTTPoison.Base
  alias Tanuki.Client

  @moduledoc """
  ## Tanuki

  At this moment this wrapper is a work in progress!

  To use this library you'll need your private token for your gitlab instance, or from
  gitlab. This can be found at <Gitlab Instance URL>profile/account

  Some functions are only to be used by admins.

  Let's get started! :)

  iex> Tanuki.Client.new(%{private_token: "T0k3n_H3r3"}, "https://yourinstance.tld/api/v3/")
  %Tanuki.Client{endpoint: "https://yourinstance.tld/api/v3/", private_token: "T0k3n_H3r3"}

  iex> Tanuki.Client.new(%{private_token: "T0k3n_H3r3"}) # Default is GitLab.com
  %Tanuki.Client{endpoint: "https://gitlab.com/api/v3/", private_token: "T0k3n_H3r3"}

  iex> Tanuki.Client.new(%{oauth_token: "T0k3n_H3r3"}) # Default is GitLab.com
  %Tanuki.Client{endpoint: "https://gitlab.com/api/v3/", private_token: nil, oauth_token: "T0k3n_H3r3"}

  With the returned client you can make requests to the endpoint specified.

  ### Conventions
  - When an id or such is needed for building the url, thus also required, it is an parameter, before passing the %Client.
  - If parameters, to be passed in the body, are required, so is the param struct. Passed after the %Client
  - If no params are used by the API endpoint, no param struct can be passed
  - In case the params are only optional, so is params.

  list -> Get a collection of what you requests
  find -> Get one, usually by id
  create -> Creates a new instance @ the endpoint
  modify -> modify the Instance @ the endpoint
  delete -> Makes it rain in South Afrika
  <action>_for_user -> as admin, create a object for that user
  """

  @doc """
  Used for all GET requests

  Shouldn't be called by you.
  """
  def get(path, client, body \\ ""), do: url(client, path) |> json_request(:get, body, client)

  @doc """
  Used for all POST requests

  Shouldn't be called by you.
  """
  def post(path, client, body \\ ""), do: url(client, path) |> json_request(:post, body, client)

  @doc """
  Used for all PUT requests

  Shouldn't be called by you.
  """
  def put(path, client, body \\ ""), do: url(client, path) |> json_request(:put, body, client)

  @doc """
  Used for all PATCH requests

  Shouldn't be called by you.
  """
  def patch(path, client, body \\ ""), do: url(client, path) |> json_request(:patch, body, client)

  @doc """
  Used for all DELETE requests

  Shouldn't be called by you.
  """
  def delete(path, client, body \\ ""), do: url(client, path) |> json_request(:delete, body, client)

  defp json_request(url, method, body, client), do: request!(method, url, body, client) |> process_response

  defp process_response(%HTTPoison.Response{status_code: 200, body: ""}),    do: ""
  defp process_response(%HTTPoison.Response{status_code: 200, body: body}),  do: body
  defp process_response(%HTTPoison.Response{status_code: 201, body: ""}),    do: ""
  defp process_response(%HTTPoison.Response{status_code: 201, body: body}),  do: body
  defp process_response(error),                                              do: {:error, error}

  defp process_request_body(body), do: Poison.encode!(body)

  defp process_request_headers(%Client{private_token: token, oauth_token: nil}) do
    [{"Accept", "application/json"}, {"PRIVATE-TOKEN", token}, {"CONTENT-TYPE", "application/json"}]
  end
  defp process_request_headers(%Client{private_token: nil, oauth_token: token}) do
    [{"Accept", "application/json"}, {"Authorization: Bearer", token}, {"CONTENT-TYPE", "application/json"}]
  end

  defp process_response_body(""), do: ""
  defp process_response_body(body), do: Poison.decode!(body, keys: :atoms)

  defp url(%Client{endpoint: endpoint}, path), do: endpoint <> path
end
