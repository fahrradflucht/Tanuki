0.2
- Add a _lot_ of endpoints

0.1
- OAuth2 support
- Improve consistency API
- Add lots of tests
- All keys in the returned structs are atoms :)
